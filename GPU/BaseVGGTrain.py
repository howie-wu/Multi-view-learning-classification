import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn as nn
from torch import optim
import os
import numpy as np
from torchvision import datasets, models, transforms
from torch.autograd import Variable
from PIL import Image
from tqdm import tqdm

class VGG16(nn.Module):
    def __init__(self, num_classes=10):
        super(VGG16, self).__init__()
        self.features = nn.Sequential(
            #1
            nn.Conv2d(3,64,kernel_size=3,padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(True),
            #2
            nn.Conv2d(64,64,kernel_size=3,padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=2,stride=2),
            #3
            nn.Conv2d(64,128,kernel_size=3,padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            #4
            nn.Conv2d(128,128,kernel_size=3,padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=2,stride=2),
            #5
            nn.Conv2d(128,256,kernel_size=3,padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            #6
            nn.Conv2d(256,256,kernel_size=3,padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            #7
            nn.Conv2d(256,256,kernel_size=3,padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=2,stride=2),
            #8
            nn.Conv2d(256,512,kernel_size=3,padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            #9
            nn.Conv2d(512,512,kernel_size=3,padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            #10
            nn.Conv2d(512,512,kernel_size=3,padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=2,stride=2),
            #11
            nn.Conv2d(512,512,kernel_size=3,padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            #12
            nn.Conv2d(512,512,kernel_size=3,padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            #13
            nn.Conv2d(512,512,kernel_size=3,padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=2,stride=2),
            nn.AvgPool2d(kernel_size=1,stride=1),
            )
        self.classifier = nn.Sequential(
            #14
            nn.Linear(512,4096),
            nn.ReLU(True),
            nn.Dropout(),
            #15
            nn.Linear(4096, 4096),
            nn.ReLU(True),
            nn.Dropout(),
            #16
            nn.Linear(4096,num_classes),
            )
        #self.classifier = nn.Linear(512, 10)
 
    def forward(self, x):
        out = self.features(x) 
#        print(out.shape)
        out = out.view(out.size(0), -1)
#        print(out.shape)
        out = self.classifier(out)
#        print(out.shape)
        return out


IMG_MEAN = [0.485, 0.456, 0.406]
IMG_STD = [0.229, 0.224, 0.225]
IMG_SIZE = 32

transform = transforms.Compose([
    transforms.Resize(IMG_SIZE),
    transforms.RandomResizedCrop(IMG_SIZE),
    transforms.RandomHorizontalFlip(),
    transforms.RandomRotation(30),
    transforms.ToTensor(),
    transforms.Normalize(IMG_MEAN, IMG_STD)
])

transform1 = transforms.Compose([
	transforms.CenterCrop((32,32)), # 只能对PIL图片进行裁剪
	transforms.ToTensor(), 
	]
)

def get_image_and_lable(dirpath):
    root = dirpath
    rooti = []

    img1 = []
    img2 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        for j in range(0, len(imgpath)):
            if j%2 == 0: 
                img1.append(imgpath[j])
                img2.append(imgpath[j+1])
                label.append(i)
    return img1, img2, label

def get_image_and_lable_test(dirpath):
    root = dirpath
    rooti = []

    img1 = []
    img2 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        # for k in range(len(imgpath)):
        #     print(imgpath[k])   
        for j in range(0, len(imgpath)):
                img1.append(imgpath[j])
                img2.append(imgpath[j])
                label.append(i)
    return img1, img2, label


def get_image_and_lable_1(dirpath):
    root = dirpath
    rooti = []
    img1 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        # for k in range(len(imgpath)):
        #     print(imgpath[k])
        for j in range(0, len(imgpath)):
                img1.append(imgpath[j])
                label.append(i)
    return img1, label

class cifar10_train_Dataset(Dataset):
    def __init__(self):
        self.Imgpath = '/root/GPU/dataset/trainWithLable'
        self.imgpath1, self.imgpath2, self.imglabel = get_image_and_lable(self.Imgpath)
        self.transform = transform
        self.imglabel = torch.from_numpy(np.array(self.imglabel))
    def __len__(self):
        return len(self.imglabel)
    def __getitem__(self,idx):
        img1_t = self.imgpath1[idx]
        img2_t = self.imgpath2[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        img2_pil = Image.open(img2_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        img2 = transform(img2_pil)
        return img1,img2,lable
    
class cifar10_test_Dataset(Dataset):
    def __init__(self,root_dir='/root/GPU/dataset/testWithLable'):
        self.Imgpath = root_dir
        self.transform = transforms
        self.imgpath1, self.imgpath2, self.imglabel = get_image_and_lable_test(root_dir)
        self.imglabel = torch.from_numpy(np.array(self.imglabel))
    def __len__(self):
        return len(self.imglabel)
    def __getitem__(self,idx):
        img1_t = self.imgpath1[idx]
        img2_t = self.imgpath2[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        img2_pil = Image.open(img2_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        img2 = transform(img2_pil)
        return img1,img2,lable


class cifar10_train_Dataset_1(Dataset):
    def __init__(self):
        self.Imgpath = '/root/GPU/dataset/trainWithLable'
        self.imgpath1, self.imglabel = get_image_and_lable_1(self.Imgpath)
        self.transform = transform
        self.imglabel = torch.from_numpy(np.array(self.imglabel))

    def __len__(self):
        return len(self.imglabel)

    def __getitem__(self, idx):
        img1_t = self.imgpath1[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        return img1, lable


class cifar10_test_Dataset_1(Dataset):
    def __init__(self, root_dir='/root/GPU/dataset/testWithLable'):
        self.Imgpath = root_dir
        self.transform = transforms
        self.imgpath1, self.imglabel = get_image_and_lable_1(root_dir)
        self.imglabel = torch.from_numpy(np.array(self.imglabel))

    def __len__(self):
        return len(self.imglabel)

    def __getitem__(self, idx):
        img1_t = self.imgpath1[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        return img1, lable

batch_size = 64
learning_rate = 1e-3  # 学习率
num_epoches = 500  # 遍历训练集的次数

# IMG_MEAN = [0.485, 0.456, 0.406]
# IMG_STD = [0.229, 0.224, 0.225]
# IMG_SIZE = 224
# transform = transforms.Compose([
#     transforms.Resize(IMG_SIZE),
#     transforms.RandomResizedCrop(IMG_SIZE),
#     transforms.RandomHorizontalFlip(),
#     transforms.RandomRotation(30),
#     transforms.ToTensor(),
#     transforms.Normalize(IMG_MEAN, IMG_STD)
# ])
# 使用自定义数据集

traindata = cifar10_train_Dataset_1()
testdata = cifar10_test_Dataset_1()
trainloader = DataLoader(traindata, batch_size=batch_size,shuffle=True)
testloader = DataLoader(testdata, batch_size=batch_size, shuffle=True)

# '''下载训练集 CIFAR-10 10分类训练集'''
# train_dataset = datasets.CIFAR10('./data', train=True, transform=transforms.ToTensor(), download=True)
# train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
# test_dataset = datasets.CIFAR10('./data', train=False, transform=transforms.ToTensor(), download=True)
# test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)

use_gpu = torch.cuda.is_available()
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = VGG16().to(DEVICE)


'''定义loss和optimizer'''
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=learning_rate)
'''训练模型'''
train_loss = []
train_acc = []
for epoch in range(num_epoches):
    print('*' * 25, 'epoch {}'.format(epoch + 1), '*' * 25)  # .format为输出格式，formet括号里的即为左边花括号的输出
    running_loss = 0.0
    running_acc = 0.0
    for i, data in tqdm(enumerate(trainloader, 1)):

        img1, label = data
        # cuda
        if use_gpu:
            img1 = img1.to(torch.float)
            img1 = img1.cuda()
            label = label.to(torch.int64)
            label = label.cuda()

        img1 = Variable(img1)
        label = Variable(label)
        # 向前传播
        out = model(img1)
        loss = criterion(out, label)
        running_loss += loss.item() * label.size(0)
        _, pred = torch.max(out, 1)  # 预测最大值所在的位置标签
        num_correct = (pred == label).sum()
        accuracy = (pred == label).float().mean()
        running_acc += num_correct.item()
        # 向后传播
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        torch.autograd.set_detect_anomaly(True)
    train_acc.append(running_acc)
    train_loss.append(running_loss)
    print('Finish {} epoch, Loss: {:.6f}, Acc: {:.6f}'.format(
        epoch + 1, running_loss / (len(traindata)), running_acc / (len(traindata))))

    model.eval()  # 模型评估
    eval_loss = 0
    eval_acc = 0
    for data in testloader:  # 测试模型
        img1, label = data
        # cuda
        if use_gpu:
            img1 = img1.to(torch.float)
            img1 = img1.cuda()
            label = label.to(torch.int64)
            label = label.cuda()
        out = model(img1)
        loss = criterion(out, label)
        eval_loss += loss.item() * label.size(0)
        _, pred = torch.max(out, 1)
        num_correct = (pred == label).sum()
        eval_acc += num_correct.item()
    print('Test Loss: {:.6f}, Acc: {:.6f}'.format(eval_loss / (len(
        testdata)), eval_acc / (len(testdata))))
    print()

# 保存模型
torch.save(model.state_dict(), './basevggcnn.pth')