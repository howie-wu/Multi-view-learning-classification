import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
import os
import numpy as np
from torchvision import datasets, models, transforms
from torch.autograd import Variable
from PIL import Image
from tqdm import tqdm

class ResidualBlock(nn.Module):
    """
    子 module: Residual Block ---- ResNet 中一个跨层直连的单元
    """
    def __init__(self, inchannel, outchannel, stride=1):
        super(ResidualBlock, self).__init__()
        self.left = nn.Sequential(
            nn.Conv2d(inchannel, outchannel, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(outchannel),
            nn.ReLU(inplace=True),
            nn.Conv2d(outchannel, outchannel, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(outchannel)
        )
        self.shortcut = nn.Sequential()
        # 如果输入和输出的通道不一致，或其步长不为 1，需要将二者转成一致
        if stride != 1 or inchannel != outchannel:
            self.shortcut = nn.Sequential(
                nn.Conv2d(inchannel, outchannel, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(outchannel)
            )

    def forward(self, x):
        out = self.left(x)
        out += self.shortcut(x)  # 输出 + 输入
        out = F.relu(out)
        return out


class ResNet(nn.Module):
    """
    实现主 module: ResNet-18
    ResNet 包含多个 layer, 每个 layer 又包含多个 residual block (上面实现的类)
    因此, 用 ResidualBlock 实现 Residual 部分，用 _make_layer 函数实现 layer
    """
    def __init__(self, ResidualBlock, num_classes=10):
        super(ResNet, self).__init__()
        self.inchannel = 64
        # 最开始的操作
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(),
        )
        self.fusion = nn.Conv2d(512, 256, 1)
        # 四个 layer， 对应 2， 3， 4， 5 层， 每层有两个 residual block
        self.layer1 = self._make_layer(ResidualBlock, 64,  2, stride=1)
        self.layer2 = self._make_layer(ResidualBlock, 128, 2, stride=2)
        self.layer3 = self._make_layer(ResidualBlock, 256, 2, stride=2)
        self.layer4 = self._make_layer(ResidualBlock, 512, 2, stride=2)
        # 最后的全连接，分类时使用
        self.fc = nn.Linear(512, num_classes)

    def _make_layer(self, block, channels, num_blocks, stride):
        """
        构建 layer, 每一个 layer 由多个 residual block 组成
        在 ResNet 中，每一个 layer 中只有两个 residual block
        """
        layers = []
        for i in range(num_blocks):
            if i == 0:  # 第一个是输入的 stride
                layers.append(block(self.inchannel, channels, stride))
            else:    # 后面的所有 stride，都置为 1
                layers.append(block(channels, channels, 1))
            self.inchannel = channels
        return nn.Sequential(*layers)  # 时序容器。Modules 会以他们传入的顺序被添加到容器中。

    def forward(self, x1, x2):
        # 最开始的处理
        out1 = self.conv1(x1)
        out2 = self.conv1(x2)
        out1 = self.layer1(out1)
        out2 = self.layer1(out2)
        out1 = self.layer2(out1)
        out2 = self.layer2(out2)
        out1 = self.layer3(out1)
        out2 = self.layer3(out2)
        out = torch.cat((out1, out2), dim=1)
        out = self.fusion(out)
        out = self.layer4(out)
        # 全连接 输出分类信息
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out

IMG_MEAN = [0.485, 0.456, 0.406]
IMG_STD = [0.229, 0.224, 0.225]
IMG_SIZE = 32

transform = transforms.Compose([
    transforms.Resize(IMG_SIZE),
    transforms.RandomResizedCrop(IMG_SIZE),
    transforms.RandomHorizontalFlip(),
    transforms.RandomRotation(30),
    transforms.ToTensor(),
    transforms.Normalize(IMG_MEAN, IMG_STD)
])

transform1 = transforms.Compose([
	transforms.CenterCrop((32,32)), # 只能对PIL图片进行裁剪
	transforms.ToTensor(), 
	]
)

def get_image_and_lable(dirpath):
    root = dirpath
    rooti = []

    img1 = []
    img2 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        for j in range(0, len(imgpath)):
            if j%2 == 0: 
                img1.append(imgpath[j])
                img2.append(imgpath[j+1])
                label.append(i)
    return img1, img2, label

def get_image_and_lable_test(dirpath):
    root = dirpath
    rooti = []

    img1 = []
    img2 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        # for k in range(len(imgpath)):
        #     print(imgpath[k])   
        for j in range(0, len(imgpath)):
                img1.append(imgpath[j])
                img2.append(imgpath[j])
                label.append(i)
    return img1, img2, label


def get_image_and_lable_1(dirpath):
    root = dirpath
    rooti = []
    img1 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        # for k in range(len(imgpath)):
        #     print(imgpath[k])
        for j in range(0, len(imgpath)):
                img1.append(imgpath[j])
                label.append(i)
    return img1, label

class cifar10_train_Dataset(Dataset):
    def __init__(self):
        self.Imgpath = '/root/GPU/dataset/trainWithLable'
        self.imgpath1, self.imgpath2, self.imglabel = get_image_and_lable(self.Imgpath)
        self.transform = transform
        self.imglabel = torch.from_numpy(np.array(self.imglabel))
    def __len__(self):
        return len(self.imglabel)
    def __getitem__(self,idx):
        img1_t = self.imgpath1[idx]
        img2_t = self.imgpath2[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        img2_pil = Image.open(img2_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        img2 = transform(img2_pil)
        return img1,img2,lable
    
class cifar10_test_Dataset(Dataset):
    def __init__(self,root_dir='/root/GPU/dataset/testWithLable'):
        self.Imgpath = root_dir
        self.transform = transforms
        self.imgpath1, self.imgpath2, self.imglabel = get_image_and_lable_test(root_dir)
        self.imglabel = torch.from_numpy(np.array(self.imglabel))
    def __len__(self):
        return len(self.imglabel)
    def __getitem__(self,idx):
        img1_t = self.imgpath1[idx]
        img2_t = self.imgpath2[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        img2_pil = Image.open(img2_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        img2 = transform(img2_pil)
        return img1,img2,lable

class cifar10_train_Dataset_1(Dataset):
    def __init__(self):
        self.Imgpath = '/root/GPU/dataset/trainWithLable'
        self.imgpath1, self.imglabel = get_image_and_lable_1(self.Imgpath)
        self.transform = transform
        self.imglabel = torch.from_numpy(np.array(self.imglabel))

    def __len__(self):
        return len(self.imglabel)

    def __getitem__(self, idx):
        img1_t = self.imgpath1[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        return img1, lable

class cifar10_test_Dataset_1(Dataset):
    def __init__(self, root_dir='/root/GPU/dataset/testWithLable'):
        self.Imgpath = root_dir
        self.transform = transforms
        self.imgpath1, self.imglabel = get_image_and_lable_1(root_dir)
        self.imglabel = torch.from_numpy(np.array(self.imglabel))

    def __len__(self):
        return len(self.imglabel)

    def __getitem__(self, idx):
        img1_t = self.imgpath1[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform(img1_pil)
        return img1, lable

batch_size = 64
learning_rate = 1e-3  # 学习率
num_epoches = 500  # 遍历训练集的次数

# IMG_MEAN = [0.485, 0.456, 0.406]
# IMG_STD = [0.229, 0.224, 0.225]
# IMG_SIZE = 224
# transform = transforms.Compose([
#     transforms.Resize(IMG_SIZE),
#     transforms.RandomResizedCrop(IMG_SIZE),
#     transforms.RandomHorizontalFlip(),
#     transforms.RandomRotation(30),
#     transforms.ToTensor(),
#     transforms.Normalize(IMG_MEAN, IMG_STD)
# ])
# 使用自定义数据集

traindata = cifar10_train_Dataset()
testdata = cifar10_test_Dataset()
trainloader = DataLoader(traindata, batch_size=batch_size,shuffle=True)
testloader = DataLoader(testdata, batch_size=batch_size, shuffle=True)

# '''下载训练集 CIFAR-10 10分类训练集'''
# train_dataset = datasets.CIFAR10('./data', train=True, transform=transforms.ToTensor(), download=True)
# train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
# testdata = datasets.CIFAR10('./data', train=False, transform=transforms.ToTensor(), download=True)
# test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)

use_gpu = torch.cuda.is_available()
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = ResNet(ResidualBlock).to(DEVICE)


'''定义loss和optimizer'''
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=learning_rate)
'''训练模型'''
train_loss = []
train_acc = []
for epoch in range(num_epoches):
    print('*' * 25, 'epoch {}'.format(epoch + 1), '*' * 25)  # .format为输出格式，formet括号里的即为左边花括号的输出
    running_loss = 0.0
    running_acc = 0.0
    for i, data in tqdm(enumerate(trainloader, 1)):

        img1,img2, label = data
        # cuda
        if use_gpu:
            img1 = img1.to(torch.float)
            img1 = img1.cuda()
            img2 = img2.to(torch.float)
            img2 = img2.cuda()
            label = label.to(torch.int64)
            label = label.cuda()

        img1 = Variable(img1)
        img2 = Variable(img2)
        label = Variable(label)
        # 向前传播
        out = model(img1,img2)
        loss = criterion(out, label)
        running_loss += loss.item() * label.size(0)
        _, pred = torch.max(out, 1)  # 预测最大值所在的位置标签
        num_correct = (pred == label).sum()
        accuracy = (pred == label).float().mean()
        running_acc += num_correct.item()
        # 向后传播
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        torch.autograd.set_detect_anomaly(True)
    train_acc.append(running_acc)
    train_loss.append(running_loss)
    print('Finish {} epoch, Loss: {:.6f}, Acc: {:.6f}'.format(
        epoch + 1, running_loss / (len(traindata)), running_acc / (len(traindata))))

    model.eval()  # 模型评估
    eval_loss = 0
    eval_acc = 0
    for data in testloader:  # 测试模型
        img1,img2, label = data
        # cuda
        if use_gpu:
            img1 = img1.to(torch.float)
            img1 = img1.cuda()
            img2 = img2.to(torch.float)
            img2 = img2.cuda()
            label = label.to(torch.int64)
            label = label.cuda()
        out = model(img1,img2)
        loss = criterion(out, label)
        eval_loss += loss.item() * label.size(0)
        _, pred = torch.max(out, 1)
        num_correct = (pred == label).sum()
        eval_acc += num_correct.item()
    print('Test Loss: {:.6f}, Acc: {:.6f}'.format(eval_loss / (len(
        testdata)), eval_acc / (len(testdata))))
    print()

# 保存模型
torch.save(model.state_dict(), './MultiResNetTrain_4.pth')