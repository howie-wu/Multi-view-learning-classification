import os
#import cv2
import shutil
import numpy as np

def getFileName(trainrootDir,testrootDir):
    train_filenames = []
    test_filenames = []
    for dirName,subDirList,filelist in os.walk(trainrootDir):
        for filename in filelist:
            if os.path.splitext(filename)[1] == '.jpg':
                train_filenames.append(dirName+'/'+filename)
    for dirName,subDirList,filelist in os.walk(testrootDir):
        for filename in filelist:
            if os.path.splitext(filename)[1] == '.jpg':
                test_filenames.append(dirName+'/'+filename)
    return train_filenames,test_filenames

def mvtrianImg(imgPath):
    train_data_destination = './train/'
    train_label_destination0 = './trainWithLable/0'
    train_label_destination1 = './trainWithLable/1'
    train_label_destination2 = './trainWithLable/2'
    train_label_destination3 = './trainWithLable/3'
    train_label_destination4 = './trainWithLable/4'
    train_label_destination5 = './trainWithLable/5'
    train_label_destination6 = './trainWithLable/6'
    train_label_destination7 = './trainWithLable/7'
    train_label_destination8 = './trainWithLable/8'
    train_label_destination9 = './trainWithLable/9'

    for item in imgPath:
        if os.path.splitext(item)[1] == '.jpg' and '0_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination0)
        elif os.path.splitext(item)[1] == '.jpg' and '1_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination1)
        elif os.path.splitext(item)[1] == '.jpg' and '2_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination2)
        elif os.path.splitext(item)[1] == '.jpg' and '3_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination3)
        elif os.path.splitext(item)[1] == '.jpg' and '4_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination4)
        elif os.path.splitext(item)[1] == '.jpg' and '5_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination5)
        elif os.path.splitext(item)[1] == '.jpg' and '6_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination6)
        elif os.path.splitext(item)[1] == '.jpg' and '7_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination7)
        elif os.path.splitext(item)[1] == '.jpg' and '8_' in os.path.splitext(item)[0]:
            shutil.copy(item, train_label_destination8)
        else:
            shutil.copy(item, train_label_destination9)
def mvtestImg(imgPath):
    test_data_destination = './test/'
    test_label_destination0 = './testWithLable/0'
    test_label_destination1 = './testWithLable/1'
    test_label_destination2 = './testWithLable/2'
    test_label_destination3 = './testWithLable/3'
    test_label_destination4 = './testWithLable/4'
    test_label_destination5 = './testWithLable/5'
    test_label_destination6 = './testWithLable/6'
    test_label_destination7 = './testWithLable/7'
    test_label_destination8 = './testWithLable/8'
    test_label_destination9 = './testWithLable/9'

    for item in imgPath:
        if os.path.splitext(item)[1] == '.jpg' and '0_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination0)
        elif os.path.splitext(item)[1] == '.jpg' and '1_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination1)
        elif os.path.splitext(item)[1] == '.jpg' and '2_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination2)
        elif os.path.splitext(item)[1] == '.jpg' and '3_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination3)
        elif os.path.splitext(item)[1] == '.jpg' and '4_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination4)
        elif os.path.splitext(item)[1] == '.jpg' and '5_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination5)
        elif os.path.splitext(item)[1] == '.jpg' and '6_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination6)
        elif os.path.splitext(item)[1] == '.jpg' and '7_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination7)
        elif os.path.splitext(item)[1] == '.jpg' and '8_' in os.path.splitext(item)[0]:
            shutil.copy(item, test_label_destination8)
        else:
            shutil.copy(item, test_label_destination9)

train_root = './train'
test_root = './test'
trainfilenames,testfilenames = getFileName(train_root,test_root)
#mvtestImg(trainfilenames)
mvtestImg(testfilenames)
