import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import os
import numpy as np
from torchvision import transforms
from torch.autograd import Variable
from PIL import Image

IMG_MEAN = [0.485, 0.456, 0.406]
IMG_STD = [0.229, 0.224, 0.225]
IMG_SIZE = 32

transform = transforms.Compose([
    transforms.Resize(IMG_SIZE),
    transforms.RandomResizedCrop(IMG_SIZE),
    transforms.RandomHorizontalFlip(),
    transforms.RandomRotation(30),
    transforms.ToTensor(),
    transforms.Normalize(IMG_MEAN, IMG_STD)
])

transform1 = transforms.Compose([
	transforms.CenterCrop((32,32)), # 只能对PIL图片进行裁剪
	transforms.ToTensor(), 
	]
)

def get_image_and_lable(dirpath):
    root = dirpath
    rooti = []

    img1 = []
    img2 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        # for k in range(len(imgpath)):
        #     print(imgpath[k])   
        for j in range(0, len(imgpath)):
            if j%2 == 0: 
                img1.append(imgpath[j])
                img2.append(imgpath[j+1])
                label.append(i)
    return img1, img2, label


def get_image_and_lable_1(dirpath):
    root = dirpath
    rooti = []
    img1 = []
    label = []
    flag = 0
    for i in range(10):
        tmp = str(i)
        rooti.append(os.path.join(root+'/'+tmp))
        imgpath = []
        for rt, dir, fname in os.walk(rooti[i]):
            for f in fname:
                tempf = str(f)
                tempd = str(rooti[i])
                imgpath.append(os.path.join(tempd+'/'+tempf))
        # for k in range(len(imgpath)):
        #     print(imgpath[k])
        for j in range(0, len(imgpath)):
                img1.append(imgpath[j])
                label.append(i)
    return img1, label

class cifar10_train_Dataset(Dataset):
    def __init__(self):
        self.Imgpath = './dataset/trainWithLable'
        self.imgpath1, self.imgpath2, self.imglabel = get_image_and_lable(self.Imgpath)
        self.transform = transform
        self.imglabel = torch.from_numpy(np.array(self.imglabel))
    def __len__(self):
        return len(self.imglabel)
    def __getitem__(self,idx):
        img1_t = self.imgpath1[idx]
        img2_t = self.imgpath2[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        img2_pil = Image.open(img2_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform1(img1_pil)
        img2 = transform1(img2_pil)
        return img1,img2,lable
    
class cifar10_test_Dataset(Dataset):
    def __init__(self,root_dir='./dataset/testWithLable'):
        self.Imgpath = root_dir
        self.transform = transforms
        self.imgpath1, self.imgpath2, self.imglabel = get_image_and_lable(root_dir)
        self.imglabel = torch.from_numpy(np.array(self.imglabel))
    def __len__(self):
        return len(self.imglabel)
    def __getitem__(self,idx):
        img1_t = self.imgpath1[idx]
        img2_t = self.imgpath2[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        img2_pil = Image.open(img2_t)
        img1_np = np.array(img1_pil)
        img1 = torch.from_numpy(np.transpose(img1_np, (2, 0, 1)))
        img2_np = np.array(img2_pil)
        img2 = torch.from_numpy(np.transpose(img2_np, (2, 0, 1)))
        return img1, img2, lable


class cifar10_train_Dataset_1(Dataset):
    def __init__(self):
        self.Imgpath = './dataset/trainWithLable'
        self.imgpath1, self.imglabel = get_image_and_lable_1(self.Imgpath)
        self.transform = transform
        self.imglabel = torch.from_numpy(np.array(self.imglabel))

    def __len__(self):
        return len(self.imglabel)

    def __getitem__(self, idx):
        img1_t = self.imgpath1[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        # img1_np = np.array(img1_pil)
        # img1 = torch.from_numpy(np.transpose(img1_np,(2, 0, 1)))
        # img2_np = np.array(img2_pil)
        # img2 = torch.from_numpy(np.transpose(img2_np,(2, 0, 1)))
        img1 = transform1(img1_pil)
        return img1, lable


class cifar10_test_Dataset_1(Dataset):
    def __init__(self, root_dir='./dataset/testWithLable'):
        self.Imgpath = root_dir
        self.transform = transforms
        self.imgpath1, self.imglabel = get_image_and_lable_1(root_dir)
        self.imglabel = torch.from_numpy(np.array(self.imglabel))

    def __len__(self):
        return len(self.imglabel)

    def __getitem__(self, idx):
        img1_t = self.imgpath1[idx]
        lable = self.imglabel[idx]
        img1_pil = Image.open(img1_t)
        img1_np = np.array(img1_pil)
        img1 = torch.from_numpy(np.transpose(img1_np, (2, 0, 1)))
        return img1, lable


device = 'cuda' if torch.cuda.is_available() else 'cpu'
traindata = cifar10_train_Dataset()
testdata = cifar10_test_Dataset()
trainloader = DataLoader(traindata, batch_size=4,shuffle=True)
testloader = DataLoader(testdata, batch_size=4, shuffle=True)
dataiter = iter(trainloader)
img1, img2,labels = dataiter.next()
print(img1.shape)