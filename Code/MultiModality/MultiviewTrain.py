import torch
import torch.nn as nn
from torch import optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from tqdm import tqdm
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms
from MultiModule import MultiVGG16
from datasets import cifar10_train_Dataset
from datasets import cifar10_test_Dataset

import


'''定义超参数'''
batch_size = 32
learning_rate = 1e-3  # 学习率
num_epoches = 100  # 遍历训练集的次数

# IMG_MEAN = [0.485, 0.456, 0.406]
# IMG_STD = [0.229, 0.224, 0.225]
# IMG_SIZE = 224
# transform = transforms.Compose([
#     transforms.Resize(IMG_SIZE),
#     transforms.RandomResizedCrop(IMG_SIZE),
#     transforms.RandomHorizontalFlip(),
#     transforms.RandomRotation(30),
#     transforms.ToTensor(),
#     transforms.Normalize(IMG_MEAN, IMG_STD)
# ])
# 使用自定义数据集

traindata = cifar10_train_Dataset()
testdata = cifar10_test_Dataset()
trainloader = DataLoader(traindata, batch_size=batch_size,shuffle=True)
testloader = DataLoader(testdata, batch_size=batch_size, shuffle=True)

# '''下载训练集 CIFAR-10 10分类训练集'''
# train_dataset = datasets.CIFAR10('./dataset/cifar10-py', train=True, transform=transforms.ToTensor(), download=True)
# train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
# test_dataset = datasets.CIFAR10('./dataset/cifar10-py', train=False, transform=transforms.ToTensor(), download=True)
# test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)

use_gpu = torch.cuda.is_available()
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = MultiVGG16().to(DEVICE)


'''定义loss和optimizer'''
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=learning_rate)
'''训练模型'''
train_loss = []
train_acc = []
for epoch in range(num_epoches):
    print('*' * 25, 'epoch {}'.format(epoch + 1), '*' * 25)  # .format为输出格式，formet括号里的即为左边花括号的输出
    running_loss = 0.0
    running_acc = 0.0
    for i, data in tqdm(enumerate(trainloader, 1)):

        img1,img2, label = data
        # cuda
        if use_gpu:
            img1 = img1.to(torch.float)
            img1 = img1.cuda()
            img2 = img2.to(torch.float)
            img2 = img2.cuda()
            label = label.to(torch.int64)
            label = label.cuda()

        img1 = Variable(img1)
        img2 = Variable(img2)
        label = Variable(label)
        # 向前传播
        out = model(img1,img2)
        loss = criterion(out, label)
        running_loss += loss.item() * label.size(0)
        _, pred = torch.max(out, 1)  # 预测最大值所在的位置标签
        num_correct = (pred == label).sum()
        accuracy = (pred == label).float().mean()
        running_acc += num_correct.item()
        # 向后传播
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        torch.autograd.set_detect_anomaly(True)
    train_acc.append(running_acc)
    train_loss.append(running_loss)
    print('Finish {} epoch, Loss: {:.6f}, Acc: {:.6f}'.format(
        epoch + 1, running_loss / (len(traindata)), running_acc / (len(traindata))))

    model.eval()  # 模型评估
    eval_loss = 0
    eval_acc = 0
    for data in testloader:  # 测试模型
        img1,img2, label = data
        # cuda
        if use_gpu:
            img1 = img1.to(torch.float)
            img1 = img1.cuda()
            img2 = img2.to(torch.float)
            img2 = img2.cuda()
            label = label.to(torch.int64)
            label = label.cuda()
        out = model(img1,img2)
        loss = criterion(out, label)
        eval_loss += loss.item() * label.size(0)
        _, pred = torch.max(out, 1)
        num_correct = (pred == label).sum()
        eval_acc += num_correct.item()
    print('Test Loss: {:.6f}, Acc: {:.6f}'.format(eval_loss / (len(
        testdata)), eval_acc / (len(testdata))))
    print()

# 保存模型
torch.save(model.state_dict(), './cnn.pth')